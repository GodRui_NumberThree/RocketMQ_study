package com.rui.mq.rocketmq.base.producer;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.common.message.Message;

import java.util.concurrent.TimeUnit;

//发送单向消息
public class OneWayProducer {

    public static void main(String[] args) throws Exception{
        DefaultMQProducer producer = new DefaultMQProducer("group1");
        producer.setNamesrvAddr("192.168.10.129:9876;192.168.10.129:9877");
        producer.setVipChannelEnabled(false);
        producer.setSendMsgTimeout(10000);
        producer.start();
        for (int i = 0; i < 3; i++) {
            Message msg = new Message("base","tag3",("Hello World 单向消息" + i).getBytes());
            producer.sendOneway(msg);
            TimeUnit.SECONDS.sleep(5);
        }
        producer.shutdown();
    }

}
