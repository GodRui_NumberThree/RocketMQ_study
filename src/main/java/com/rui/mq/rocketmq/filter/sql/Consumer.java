package com.rui.mq.rocketmq.filter.sql;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.MessageSelector;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;

public class Consumer {

    public static void main(String[] args) throws Exception{
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("group1");
        consumer.setNamesrvAddr("192.168.10.129:9876;192.168.10.129:9877");
        consumer.setVipChannelEnabled(false);
        consumer.subscribe("FilterSqlTopic", MessageSelector.bySql("i>5"));
        consumer.registerMessageListener((MessageListenerConcurrently) (msgs,context) ->{
            for(MessageExt message : msgs){
                System.out.println("consumeThread = " + Thread.currentThread().getName() + "，" + new String(message.getBody()));
            }
            return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
        });
        consumer.start();
        System.out.println("消费者启动");
    }
}
