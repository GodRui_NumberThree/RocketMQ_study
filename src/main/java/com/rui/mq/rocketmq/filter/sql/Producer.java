package com.rui.mq.rocketmq.filter.sql;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

import java.util.concurrent.TimeUnit;

public class Producer {

    public static void main(String[] args) throws Exception{
        DefaultMQProducer producer = new DefaultMQProducer("group1");
        producer.setNamesrvAddr("192.168.10.129:9876;192.168.10.129:9877");
        producer.setVipChannelEnabled(false);
        producer.setSendMsgTimeout(10000);
        producer.start();
        for (int i = 0; i < 10; i++) {
            Message msg = new Message("FilterSqlTopic","tag1",("Hello World" + i).getBytes());
            msg.putUserProperty("i",String.valueOf(i));
            SendResult result = producer.send(msg);
            System.out.println("发送结果：" + result);
            TimeUnit.SECONDS.sleep(1);
        }
        producer.shutdown();
    }
}
