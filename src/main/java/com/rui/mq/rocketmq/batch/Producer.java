package com.rui.mq.rocketmq.batch;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.common.message.Message;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Producer {

    public static void main(String[] args) throws Exception{
        DefaultMQProducer producer = new DefaultMQProducer("group1");
        producer.setNamesrvAddr("192.168.10.129:9876;192.168.10.129:9877");
        producer.setVipChannelEnabled(false);
        producer.setSendMsgTimeout(10000);
        producer.start();
        List<Message> msgs = new ArrayList<>();
        Message msg1 = new Message("BatchTopic","tag1",("Hello World" + 1).getBytes());
        Message msg2 = new Message("BatchTopic","tag1",("Hello World" + 2).getBytes());
        Message msg3 = new Message("BatchTopic","tag1",("Hello World" + 3).getBytes());
        msgs.add(msg1);
        msgs.add(msg2);
        msgs.add(msg3);
        SendResult result = producer.send(msgs);
        System.out.println("发送结果：" + result);
        TimeUnit.SECONDS.sleep(1);
        producer.shutdown();
    }
}
